import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.lang.Math;
import java.util.Map;

public class Main {
	public static void main(String[] args){
		Scanner reader= new Scanner(System.in);
		int n= reader.nextInt();
		String command;
		String input;
		Formula f;
		TreeSet<String> symbolTable;
		for (int i=0;i<n;i++){
			command = reader.next();
			input =reader.nextLine().substring(1);

			String[] slicedInput = parseInput(command, input);
			if(slicedInput == null){
				f = parseSentence(input);	
			}else {
				f = parseSentence(slicedInput[0]);
			}
			symbolTable = f.getSymbolTable();
			
			if(command.equals("sub")){ 
				printSet(f.getAllSubformula());
				System.out.println(input + " has " + f.getSubformula().size() + " different subformulas" );
			}else if(command.equals("tt")){
				for (String symbol : symbolTable) {
					System.out.print(symbol+" ");
				}
				System.out.println("F");

				for (int bitmask =0; bitmask < Math.pow(2, symbolTable.size()) ; bitmask++ ) {
					Map<String, Boolean> model = createModel(symbolTable, bitmask);
					for(Map.Entry<String, Boolean> entry : model.entrySet()) {
					    String key = entry.getKey();
					    Boolean value = entry.getValue();

					    if(value) System.out.print(1);
					    else System.out.print(0);

					    // print space 
					    for(int j=0; j < key.length(); j++){
					    	System.out.print(" ");
					    }
					}
					boolean truthValue = f.getTruthValue(model);
					if(truthValue) System.out.println(1);
					else System.out.println(0);
				}
			}else if(command.equals("ecnf")){
				Formula ecnf = f.toNNF().NNFtoCNF();
				String output;
				if(ecnf == null){
					output = "{}";
				}else {
					output = ecnf.getClauseCollection().toString();
				}
				output = (output.replace("[","{")).replace("]","}");
				System.out.println(output);

			}else if(command.equals("res")){
				Formula ecnf = f.toNNF().NNFtoCNF();
				if(ecnf == null){
					System.out.println("0: {}");
					System.out.println("unsatisfiable");
				}else {
					HashSet<Clause> clauseCollection=ecnf.getClauseCollection();
					if(res(clauseCollection)){
						System.out.println("unsatisfiable");
					}else{
						System.out.println("satisfiable");
					}
				}
				
			}else if(command.equals("valid")){
				input = "~"+input;
				f = parseSentence(input);
				Formula ecnf = f.toNNF().NNFtoCNF();
				if(ecnf == null){
					System.out.println("Check if " + input + " is unsatisfiable" );
					System.out.println("0: {}");
					System.out.println("unsatisfiable");
					System.out.println(input.substring(1) + " is valid");
				}else {
					HashSet<Clause> clauseCollection=ecnf.getClauseCollection();
					System.out.println("Check if " + input + " is unsatisfiable" );
					if(res(clauseCollection)){
						System.out.println("unsatisfiable");
						System.out.println(input.substring(1) + " is valid");
					}else{
						System.out.println("satisfiable");
						System.out.println(input.substring(1) + " is not valid");
					}
				}
			}else if(command.equals("ent")){
				String entailment = "("+slicedInput[0]+" & ~" +slicedInput[1] + ")";
				Formula entailmentFormula = parseSentence(entailment); 
				Formula ecnf = entailmentFormula.toNNF().NNFtoCNF();
				if(ecnf == null){
					System.out.println("Check if "+ entailment + " is unsatisfiable" );
					System.out.println("0: {}");
					System.out.println("unsatisfiable");
					System.out.println(slicedInput[0] + " entails " + slicedInput[1] );
				}else {
					HashSet<Clause> clauseCollection = ecnf.getClauseCollection();
					System.out.println("Check if "+ entailment + " is unsatisfiable" );
					if(res(clauseCollection)){
						System.out.println("unsatisfiable");
						System.out.println(slicedInput[0] + " entails " + slicedInput[1] );
					}else{
						System.out.println("satisfiable");
						System.out.println(slicedInput[0] + " does not entail " + slicedInput[1] );
					}
				}
			}else {
				System.out.println("Unknown input");
			}
		}
		reader.close();
	}

	static String[] parseInput(String command, String input){
		if(command.equals("ent")){
			char first = input.charAt(0);
			char last = input.charAt(input.length()-1);
			String[] slicedInput = new String[2];
			int index = 0;
			if ((first >= 'a' && first <= 'z') || (first >= 'A' && first <= 'Z')){
				index = input.indexOf(' ');
				slicedInput[0] = input.substring(0,index);
				slicedInput[1] = input.substring(index+1,input.length());
			} else if ((last >= 'a' && last <= 'z') || (last >= 'A' && last <= 'Z') || (last >= '0' && last <= '9')){
				index = input.lastIndexOf(' ');
				slicedInput[0] = input.substring(0,index);
				slicedInput[1] = input.substring(index+1,input.length());
			}else {
				input = input.replace(") (",")#(");
				slicedInput = input.split("#");
			}

			return slicedInput;

		}else {
			return null;
		}
	}

//	return true if unsatisfiable
	static boolean res(HashSet<Clause> clause){
		String replacedClaused = clause.toString().replace("[","{").replace("]","}");
		System.out.println("CNF :"+replacedClaused);
		ArrayList<Clause> temp= new ArrayList<Clause>();
		temp.addAll(clause);
		int i=1;
		int j=0;
		int n=0;
		Clause combined;
		while(n<temp.size()){
			String tempN = temp.get(n).toString().replace("[","{").replace("]","}");
			System.out.println(n+": "+tempN);
			n++;
		}
		while(i<temp.size()){
			while(j<i){
				combined=temp.get(i).res(temp.get(j));
				if(combined!=null){
					if(!temp.contains(combined)){
						temp.add(combined);
						String combinedString = combined.toString().replace("[","{").replace("]","}");
						System.out.println(n++ +": "+combinedString+" --- "+"("+i+","+j+")");
					}
					// else {
					// 	System.out.println("... " + "("+i+","+j+")" +" yields clause at " + temp.indexOf(combined));
					// }
				}else{
					System.out.println(n++ +": {}"+" --- "+"("+i+","+j+")");
					return true;
				}
				j++;
			}
			j=0;
			i++;
		}
		return false;
	}
	// create a model from symbol table and assign value according to bitmask
	static TreeMap<String,Boolean> createModel(TreeSet<String> symbolTable, int mask){
		TreeMap<String, Boolean> ret= new TreeMap<String, Boolean>();
		String binaryMask = Integer.toString(mask,2);
		if(binaryMask.length() < symbolTable.size()){
			int diff = symbolTable.size() - binaryMask.length();
			for(int i = 0; i < diff; i++){
				binaryMask = "0" + binaryMask;
			}
		}
		int i =0;
		Iterator<String> iter=symbolTable.iterator();
		while(iter.hasNext()){
			if(binaryMask.charAt(i) == '1'){
				ret.put(iter.next(), true);
			}else {
				ret.put(iter.next(), false);
			}
			i++;
		}
		return ret;
		
	}
	
	static Tuple getOperant(String input){
		String simplified= input.replace("<->", "=");
		simplified= simplified.replace("->", "+");
		char ret='!';
		int count=0;
		int pos=0;
		if (simplified.charAt(0)=='~'){
			ret='~';
			if(simplified.charAt(1)=='(' & getOperant(simplified.substring(1)).symbol=='~'){
				pos++;
			}
			return new Tuple(ret,pos);
		}
		for (int i=0;i<simplified.length() && (ret=='!'|ret=='~');i++){
			if (simplified.charAt(i)=='('){
				count++;
			}else if (simplified.charAt(i)==')'){
				count--;

			}
			if (count== 1 && (simplified.charAt(i)=='|' || simplified.charAt(i)=='&' || simplified.charAt(i)=='=' || simplified.charAt(i)=='+')){
				ret=simplified.charAt(i);
				pos=i;
			}
			if(ret=='!' & simplified.charAt(i)=='~'){
				ret = '~';
				pos=i;
			}
		}
		return new Tuple(ret,pos);
	}
	
	
	static void printSet(ArrayList<String> set){
		Iterator<String> iter=set.iterator();
		while(iter.hasNext()){
			System.out.println(iter.next());
		}
	}
	
	static Formula parseSentence(String input){

		String simplified= input.replace("<->", "=");
		simplified= simplified.replace("->", "+");
		Operant disjunction= new Disjuction();
		Operant conjunction= new Conjuction();
		Tuple tuple=getOperant(input);
		char symbol = tuple.symbol;
		int post= tuple.position;
		if (symbol=='!'){
			// remove space before atomic formula
			return new AtomicFormula(simplified.replace(" ", ""));
		}
		else if (symbol=='~'){
			return new NegatedFormula(parseSentence(simplified.substring(tuple.position+1,simplified.length()-tuple.position)));
		}else{
			String leftString=simplified.substring(1, post-1);
			String rightString=simplified.substring(post+2, simplified.length()-1);
			Formula leftFormula= parseSentence(leftString);
			Formula rightFormula= parseSentence(rightString);
			if(symbol=='+'){
				return new ComplexFormula(new NegatedFormula(leftFormula), rightFormula, disjunction);
			}else if(symbol=='='){
				ComplexFormula left=new ComplexFormula(new NegatedFormula(leftFormula), rightFormula, disjunction);
				ComplexFormula right=new ComplexFormula(leftFormula, new NegatedFormula(parseSentence(rightString)), disjunction);
				return new ComplexFormula(left, right, conjunction);
			}else if (symbol=='|'){
				return new ComplexFormula(leftFormula, rightFormula, disjunction);
			}else if (symbol=='&'){
				return new ComplexFormula(leftFormula, rightFormula, conjunction);
			}
		}
		return null;
	}
}

// Class for tuple of symbol and their position in simplified string
class Tuple{
	public char symbol;
	public int position;
	public Tuple(char symbol, int pos){
		this.symbol=symbol;
		this.position=pos;
	}
}

// Inteface to create propotitional logic operant
interface Operant{
	boolean operate(boolean a, boolean b);
	char getSymbol();
}

class Conjuction implements Operant{
	private static char symbol='&';
	public char getSymbol(){
		return symbol;
	}
	public boolean operate(boolean a,boolean b){
		return a && b;
	}
}

class Disjuction implements Operant{
	private static char symbol='|';
	public char getSymbol(){
		return symbol;
	}
	public boolean operate(boolean a,boolean b){
		return a || b;
	}
}
