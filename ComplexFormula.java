import java.util.HashSet;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.Map;

import javax.management.InstanceAlreadyExistsException;

public class ComplexFormula implements Formula{
	Formula leftChildFormula;
	Formula rightChildFormula;
	Operant op;
	public ComplexFormula(Formula left, Formula right, Operant op){
		leftChildFormula=left;
		rightChildFormula= right;
		this.op=op;
	}
	
	public boolean getTruthValue(Map<String,Boolean> map){
		return op.operate(leftChildFormula.getTruthValue(map),rightChildFormula.getTruthValue(map));
	}
	public LinkedHashSet <String> getSubformula(){
		LinkedHashSet <String> ret = new LinkedHashSet <String>();
		ret.add(this.toString());
		ret.addAll(leftChildFormula.getSubformula());
		ret.addAll(rightChildFormula.getSubformula());
		return ret;
	} 
	public ArrayList<String> getAllSubformula(){
		ArrayList<String> ret = new ArrayList<String>();
		ret.add(this.toString());
		ret.addAll(leftChildFormula.getAllSubformula());
		ret.addAll(rightChildFormula.getAllSubformula());
		return ret;
	}
	public TreeSet<String> getSymbolTable(){
		TreeSet<String> ret = new TreeSet<String>();
		ret.addAll(leftChildFormula.getSymbolTable());
		ret.addAll(rightChildFormula.getSymbolTable());
		return ret;
	}
	public String toString(){
		return "("+leftChildFormula+" "+ op.getSymbol()+" "+rightChildFormula+")"; 
	}
	public Formula toNNF(){
		Formula left=leftChildFormula.toNNF();
		Formula right=rightChildFormula.toNNF();
		return new ComplexFormula(left, right, op);
	}
	public Formula NNFtoCNF(){
		Formula leftCNF=leftChildFormula.NNFtoCNF();
		Formula rightCNF=rightChildFormula.NNFtoCNF();
		
		if(leftCNF == null ){
			return rightCNF;
		}else if (rightCNF == null){
			return leftCNF;
		}else if(leftCNF.equals(rightCNF)){
			return leftCNF;
		}else{
			if (op instanceof Disjuction){
				if (leftCNF instanceof ComplexFormula && ((ComplexFormula) leftCNF).op instanceof Conjuction){
					ComplexFormula left=(ComplexFormula) leftCNF;
					Formula leftLeft= left.leftChildFormula;
					Formula leftRight= left.rightChildFormula;
					Formula newLeft= new ComplexFormula(leftLeft, rightCNF, op).NNFtoCNF();
					Formula newRight= new ComplexFormula(leftRight, rightCNF, op).NNFtoCNF();					
					
					if(newRight==null){
						if(newLeft==null){
							return null;
						}else{
							return newLeft;
						}
					}else{
						if(newLeft==null){
							return newRight;
						}else{
							return new ComplexFormula(newLeft, newRight, new Conjuction());
						}
					}				
				}
				
				else if(rightCNF instanceof ComplexFormula && ((ComplexFormula) rightCNF).op instanceof Conjuction){
					ComplexFormula right=(ComplexFormula) rightCNF;
					Formula rightLeft= right.leftChildFormula;
					Formula rightRight= right.rightChildFormula;
					Formula newLeft= new ComplexFormula(rightLeft, leftCNF, op).NNFtoCNF();
					Formula newRight= new ComplexFormula(rightRight, leftCNF, op).NNFtoCNF();
				
					if(newRight==null){
						if(newLeft==null){
							return null;
						}else{
							return newLeft;
						}
					}else{
						if(newLeft==null){
							return newRight;
						}else{
							return new ComplexFormula(newLeft, newRight, new Conjuction());
						}
					}
				}
				else{
					Clause temp=new ComplexFormula(leftCNF, rightCNF, op).toCNFClause();
					if(temp!=null){
						return temp.toFormula();
					}else{
						return null;
					}
				}
			}else {
				if(op instanceof Disjuction){
					Clause temp=this.toCNFClause();
					if(temp!=null){
						return temp.toFormula();
					}else{
						return null;
					}	
				}
				else{
					return new ComplexFormula(leftCNF, rightCNF, op);
				}
			}
		}	 
	}
	private Clause toCNFClause(){
		Clause leftClause;
		Clause rightClause;
		if (leftChildFormula instanceof ComplexFormula){
			leftClause= ((ComplexFormula) leftChildFormula).toCNFClause();
		}else{
			leftClause=new Clause(leftChildFormula);
		}
		
		if (rightChildFormula instanceof ComplexFormula){
			rightClause= ((ComplexFormula) rightChildFormula).toCNFClause();
		}else{
			rightClause=new Clause(rightChildFormula);
		}
		
		if (leftClause!=null){
			Clause temp=leftClause.combine(rightClause);
			return temp;
		}else{
			return null;
		}
	}
	public HashSet<Clause> getClauseCollection(){
		HashSet<Clause> ret= new HashSet<Clause>();
		if (op instanceof Conjuction){
			HashSet<Clause> left=leftChildFormula.getClauseCollection();
			HashSet<Clause> right=rightChildFormula.getClauseCollection();
			ret.addAll(left);
			ret.addAll(right);
		}else{
			ret.add(toCNFClause());
		}
		return ret;
	}
	public Formula negated(){
		Formula left= new NegatedFormula(leftChildFormula);
		Formula right= new NegatedFormula(rightChildFormula);
		Operant oper; 
		if(op instanceof Disjuction){
			oper= new Conjuction();
		}else {
			oper= new Disjuction();
		}
		return new ComplexFormula(left, right, oper);
	}
	@Override
	public boolean equals(Object other){
		if(other instanceof ComplexFormula){
			ComplexFormula otherFormula=(ComplexFormula) other;
			if (otherFormula.leftChildFormula.equals(leftChildFormula)&& otherFormula.rightChildFormula.equals(rightChildFormula)){
				return true;
			}else if (otherFormula.leftChildFormula.equals(rightChildFormula)&& otherFormula.rightChildFormula.equals(leftChildFormula)){
				return true;
			}
			return false;
		}
		return false;
	}
	@Override
	public int hashCode() {
		int leftHash=leftChildFormula.hashCode();
		int rightHash=rightChildFormula.hashCode();
	    return leftHash+rightHash;
	}
}
