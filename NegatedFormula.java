import java.util.HashSet;
import java.util.TreeSet;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.Map;

public class NegatedFormula implements Formula {
	Formula childFormula; 
	public NegatedFormula(Formula child){
		childFormula= child;
	}
	public boolean getTruthValue(Map<String,Boolean> map){
		return !childFormula.getTruthValue(map);
	}
	public LinkedHashSet <String> getSubformula(){
		LinkedHashSet <String> ret = new LinkedHashSet <String>();
		ret.add(this.toString());
		ret.addAll(childFormula.getSubformula());
		return ret;
	}
	public ArrayList <String> getAllSubformula(){
		ArrayList <String> ret = new ArrayList <String>();
		ret.add(this.toString());
		ret.addAll(childFormula.getAllSubformula());
		return ret;
	}
	public TreeSet<String> getSymbolTable(){
		TreeSet<String> ret = new TreeSet<String>();
		ret.addAll(childFormula.getSymbolTable());
		return ret;
	};
	public String toString(){
		if(childFormula instanceof NegatedFormula){
			return "~("+childFormula+")";
		}else
			return "~"+childFormula;
	}
	public Formula toNNF(){
		if(childFormula instanceof AtomicFormula){
			return this;
		}
		return childFormula.negated().toNNF();
	}
	public Formula NNFtoCNF(){
		return new NegatedFormula(childFormula.NNFtoCNF());
	}
	public Formula negated(){
		return childFormula;
	}
	public HashSet<Clause> getClauseCollection(){
		HashSet<Clause> ret= new HashSet<Clause>();
		ret.add(new Clause(this));
		return ret;
	}
	
	@Override
	public boolean equals(Object other){
		return(other.toString().equals(toString()));
	}
	public int hashCode() {
	    return toString().hashCode();
	}
}
