import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class Clause {
	HashSet<Formula> formulaCollector=new HashSet<Formula>();
	public Clause(HashSet<Formula> clause){
		formulaCollector=clause;
	}
	public Clause(Formula formula){
		formulaCollector.add(formula);
	}
	public Clause combine(Clause other){
		if (other != null){
			formulaCollector.addAll(other.formulaCollector);	
			return simplify();	
		}else{
			return null;
		}
	}
	public Clause res(Clause other){
		HashSet<Formula> formula=new HashSet<Formula>();
		formula.addAll(other.formulaCollector);
		formula.addAll(formulaCollector);
		return resolution(formula);	
	}
	private Clause resolution(HashSet<Formula> formula){
		HashSet<Formula> temp=new HashSet<Formula>();
		Iterator<Formula> it=formula.iterator();
		Formula fTemp;
		while(it.hasNext()){
			fTemp=it.next();
			if(temp.contains(fTemp.negated())){
				temp.remove(fTemp.negated());
			}else{
				temp.add(fTemp);
			}
		}
		if(temp.size()>0)
			return new Clause(temp);
		else
			return null;
	}
	private Clause simplify(){
		Iterator<Formula> it= formulaCollector.iterator();
		Formula temp;
		while (it.hasNext()){
			temp=it.next();
			if(temp instanceof AtomicFormula){
				if (formulaCollector.contains(new NegatedFormula(temp)))
					return null;
			}else if(temp instanceof NegatedFormula){
				if(formulaCollector.contains(((NegatedFormula) temp).childFormula)){
					return null;
				}
			}
		}
		return this;
	}
	public Formula toFormula(){
		Iterator<Formula> it= formulaCollector.iterator();
		Formula ret=it.next();
		while(it.hasNext()){
			ret= new ComplexFormula(ret, it.next(), new Disjuction());
		}
		return ret;
	}
	public String toString(){
		return formulaCollector.toString();
	}
	@Override
	public boolean equals(Object other){
		if(other instanceof Clause){
			HashSet<Formula> temp= new HashSet<Formula>();
			temp.addAll(((Clause) other).formulaCollector);
			if(temp.size()!=formulaCollector.size()){
				return false;
			}
			
			int n=temp.size();
			temp.addAll(formulaCollector);
			if(n!=temp.size()){
				return false;
			}
			return true;
		}
		return false;
	}
	@Override
	public int hashCode() {
		Iterator<Formula> it=formulaCollector.iterator();
		int ret=0;
		while(it.hasNext()){
			ret+= it.next().hashCode();
		}
		return 0;
	}
}
