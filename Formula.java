import java.util.HashSet;
import java.util.TreeSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.ArrayList;

public interface Formula {
	boolean getTruthValue(Map<String,Boolean> map);
	LinkedHashSet<String> getSubformula();
	ArrayList<String> getAllSubformula();
	String toString();
	TreeSet<String> getSymbolTable();
	Formula toNNF();
	Formula negated();
	Formula	NNFtoCNF();
	HashSet<Clause> getClauseCollection();
}
