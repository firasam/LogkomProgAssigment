import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.TreeSet;
import java.util.ArrayList;

public class AtomicFormula implements Formula {
	String symbol;
	public AtomicFormula(String symbol){
		this.symbol=symbol;
	}
	public boolean getTruthValue(Map<String,Boolean> map){
		return map.get(symbol);
	}
	
	public LinkedHashSet <String> getSubformula(){
		LinkedHashSet <String> ret= new LinkedHashSet <String>();
		ret.add(this.toString());
		return ret;
	}
	public ArrayList <String> getAllSubformula(){
		ArrayList <String> ret= new ArrayList <String>();
		ret.add(this.toString());
		return ret;
	}
	public TreeSet<String> getSymbolTable(){
		TreeSet<String> ret = new TreeSet<String>();
		ret.add(symbol);
		return ret;
	};
	public String toString(){
		return symbol;
	}
	public Formula toNNF(){
		return this;
	}
	public Formula negated(){
		return new NegatedFormula(this);
	}
	public Formula NNFtoCNF(){
		return this;
	}
	public HashSet<Clause> getClauseCollection(){
		HashSet<Clause> ret= new HashSet<Clause>();
		ret.add(new Clause(this));
		return ret;
	}
	@Override
	public boolean equals(Object other){
		return(other.toString().equals(toString()));
	}
	@Override
	public int hashCode() {
	    return toString().hashCode();
	}
}
